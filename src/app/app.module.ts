// Core Library
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

// External Library
import { TabsModule } from 'ngx-bootstrap/tabs';

// My app class
import { AppComponent } from './app.component';
import { SharedService } from './shared-service';
import { LoginComponent } from './login/login.component';
import { UserService } from './login/user.service';
import { HomeComponent } from './home/home.component';
import { MessageComponent } from './message/message.component';
import { ErrorHandlesComponent } from './error-handles/error-handles.component';

// 3rd Party Library
import { ModalModule } from 'ngx-bootstrap/modal';
import { ErrorHandlesService } from './error-handles/error-handles.service';
import { HomeModule } from './home/home.module';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'home/message', component: MessageComponent },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorHandlesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    HomeModule
  ],
  providers: [
    SharedService,
    UserService,
    ErrorHandlesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
