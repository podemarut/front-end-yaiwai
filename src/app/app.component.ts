import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SharedService } from './shared-service';
import { MessageModel } from './models/message';
import { Subject } from 'rxjs';
import { UserService } from './login/user.service';
import { Router } from '@angular/router';
import { UserModel } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private activeUser: UserModel;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.userService.userModel.subscribe((data: any) => {
      this.activeUser = data;
    });
  }

  signOut() {
    this.userService.signout();
    this.router.navigateByUrl('/');
  }

  isAuth() {
    if (this.activeUser != null) {
      return true;
    } else {
      return false;
    }
  }
}
