import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx'
import { MessageModel } from './models/message';
import { ErrorHandlesService } from './error-handles/error-handles.service';
import { ErrorModel } from './models/error';
@Injectable()
export class SharedService {
    private _url: string = 'http://localhost:3000';
    private _httpHeader: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Method': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    })
    constructor(private http: HttpClient, private errorHandlesService: ErrorHandlesService) {

    }

    getToken() {
        return localStorage.getItem('token') ? '?token=' + localStorage.getItem('token') : '';
    }

    getMessages(): Observable<any> {
        const token = this.getToken();
        let errRes: ErrorModel = new ErrorModel();
        return this.http.get(this._url + '/message' + token)
            .map((response: any) => {
                console.log('Get msg', response);
                return response;
            }).catch((error: Response) => {
                errRes.statusCode = error.status;
                this.errorHandlesService.setErrorStatus(errRes);
                return Observable.throw('Error');
            });
    }

    insertMessage(message: any): Observable<any> {
        const body = JSON.stringify(message);
        const token = this.getToken();
        let result;
        let errRes: ErrorModel = new ErrorModel();
        return this.http.post(this._url + '/message' + token, body, { headers: this._httpHeader })
            .map((response: Response) => result = response, console.log(result))
            .catch((error: Response) => {
                errRes.statusCode = error.status;
                this.errorHandlesService.setErrorStatus(errRes);
                return Observable.throw('Error');
            });
    }

    patchMessage(message: MessageModel): Observable<any> {
        const body = JSON.stringify(message);
        const token = this.getToken();
        let result;
        let errRes: ErrorModel = new ErrorModel();
        return this.http.patch(this._url + '/message/' + message.messageId + token, body, { headers: this._httpHeader })
            .map((response: Response) => result = response, console.log(result))
            .catch((error: Response) => {
                errRes.statusCode = error.status;
                this.errorHandlesService.setErrorStatus(errRes);
                return Observable.throw('Error');
            });
    }

    deleteMessage(messageId: number): Observable<any> {
        const token = this.getToken();
        let result;
        let errRes: ErrorModel = new ErrorModel();
        return this.http.delete(this._url + '/message/' + messageId + token)
            .map((response: Response) => {
                return response;
            })
            .catch((error: any) => {
                errRes.statusCode = error.status;
                this.errorHandlesService.setErrorStatus(errRes);
                return Observable.throw('Error');
            });
    }
}