import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserModel } from '../models/user';
import { UserService } from './user.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub_regis.unsubscribe();
  }

  private sub_regis: any = new Subject();
  private sub_signin: any = new Subject();

  login_form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  register_form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required)
  })

  submitLogin() {
    let user = new UserModel();
    user.username = this.login_form.get('username').value;
    user.password = this.login_form.get('password').value;

    this.sub_signin = this.userService.signin(user).subscribe(
      (data: any) => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('userId', data.userId);

        let userModel = new UserModel();
        userModel.firstName = data.firstName;
        userModel.lastName = data.lastName;
        this.userService.setUserModel(userModel);
        this.router.navigateByUrl('/home/message');
      },
      (error: any) => {
        console.error('Logged in failed', error);
      }
    );

    this.login_form.reset();
  }

  submitRegister() {
    let user = new UserModel();
    user.username = this.register_form.get('username').value;
    user.firstName = this.register_form.get('firstName').value;
    user.lastName = this.register_form.get('lastName').value;
    user.password = this.register_form.get('password').value;
    user.email = this.register_form.get('email').value;

    this.sub_regis = this.userService.signup(user).subscribe(
      (data: any) => {
        console.log('Created User', data);
      },
      (error: any) => {
        console.error('Cannot create user', error);
      }
    );
  }
}
