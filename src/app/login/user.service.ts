import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx'
import { UserModel } from '../models/user';
import { BehaviorSubject } from 'rxjs/Rx';
import { ErrorModel } from '../models/error';
import { ErrorHandlesService } from '../error-handles/error-handles.service';

@Injectable()
export class UserService {
    private _url: string = 'http://localhost:3000';
    private _httpHeader: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Method': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

    private userSource: any = new BehaviorSubject<UserModel>(null);
    userModel: any = this.userSource.asObservable();
    constructor(private http: HttpClient, private errorHandlesService: ErrorHandlesService) {

    }

    setUserModel(user: UserModel) {
        this.userSource.next(user);
    }

    signup(user: UserModel) {
        const body = JSON.stringify(user);
        let result;
        return this.http.post(this._url + '/user', body, { headers: this._httpHeader })
            .map((response: Response) => result = response, console.log(result));
    }

    signin(user: UserModel) {
        const body = JSON.stringify(user);
        let result;
        return this.http.post(this._url + '/user/signin', body, { headers: this._httpHeader })
            .map((response: Response) => {
                return response;
            })
            .catch((error: Response) => {
                return Observable.throw(error);
            });
    }

    signout() {
        localStorage.clear();
        this.setUserModel(null);
    }
}