import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared-service';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageModel } from '../models/message';
import { Router } from '@angular/router';
import { UserService } from '../login/user.service';
import { UserModel } from '../models/user';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  constructor(private sharedService: SharedService, private router: Router, private userService: UserService) {

  }

  ngOnInit() {
    this.getMessage();

    this.sub_userModel = this.userService.userModel.subscribe(
      (data: any) => {
        this.userModel = data;
      });
  }
  messageList: Array<any> = [];
  sub_get: any = new Subject();
  sub_insert: any = new Subject();
  sub_userModel: any = new Subject();
  userModel: UserModel = null;

  message_form = new FormGroup({
    message: new FormControl('', Validators.required)
  })

  submitForm() {
    let params = new MessageModel();
    params.content = this.message_form.get('message').value;
    params.user = 'Pode';
    this.addMessage(params);
    this.message_form.reset();
  }

  addMessage(params: any) {
    this.sub_insert = this.sharedService.insertMessage(params)
      .subscribe(
        (data: any) => {
          data['user'] = this.userModel;
          this.messageList.push(data);
        },
        (error: any) => {
          console.error("Error: ", error)
        }
      );
  }

  getMessage() {
    this.sub_get = this.sharedService.getMessages()
      .subscribe((data: any) => {
        this.messageList = data.obj;
      });
  }

  updateMessage(messageObj: any) {
    let params = new MessageModel();
    params.content = messageObj.content + "x";
    params.messageId = messageObj._id;
    this.sharedService.patchMessage(params).subscribe(
      (data: any) => {

      }, (error: any) => {

      });
  }

  deleteMessage(messageObj: any, idx?: number) {
    this.sharedService.deleteMessage(messageObj._id).subscribe(
      (data: any) => {
        this.messageList.splice(idx, 1);
      }, (error: any) => {

      });
  }

  ngOnDestroy(): void {
    this.sub_insert.unsubscribe();
    this.sub_userModel.unsubscribe();
  }

}
