import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ErrorHandlesService } from './error-handles.service';
import { ErrorModel } from '../models/error';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-handles',
  templateUrl: './error-handles.component.html',
  styleUrls: ['./error-handles.component.css']
})
export class ErrorHandlesComponent implements OnInit {
  @ViewChild('errorHandlesModalRef') errorHandlesModalRef: TemplateRef<any>;
  modalRef: BsModalRef;
  constructor(private errorHandlesService: ErrorHandlesService, private modalService: BsModalService, private router: Router) { }

  ngOnInit() {
    this.errorHandlesService.errorHandles
      .subscribe((data: ErrorModel) => {
        if (data != null) {
          let statusCode: number = data.statusCode;
          switch (statusCode) {
            case 400:
              // Bad Request
              break;
            case 401:
              // Unauthorized
              this.router.navigateByUrl('/');
              break;
            case 500:
              // Internal Server Error
              this.showModal();
              break;
          }
        }
      });
  }

  showModal() {
    this.modalRef = this.modalService.show(this.errorHandlesModalRef);
  }
  hideModal() {
    this.modalRef.hide();
  }



}
