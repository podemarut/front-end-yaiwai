import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorHandlesComponent } from './error-handles.component';

describe('ErrorHandlesComponent', () => {
  let component: ErrorHandlesComponent;
  let fixture: ComponentFixture<ErrorHandlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorHandlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorHandlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
