import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ErrorModel } from '../models/error';

@Injectable()
export class ErrorHandlesService {

    private errorStatusSource = new BehaviorSubject<ErrorModel>(null);
    errorHandles: any = this.errorStatusSource.asObservable();

    constructor() {

    }

    setErrorStatus(errorObj: ErrorModel) {
        this.errorStatusSource.next(errorObj);
    }
}