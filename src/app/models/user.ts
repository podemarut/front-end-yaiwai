
export class UserModel {
    userId: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    message: string;
}