export class ErrorModel {
    statusCode: number;
    title: string;
    details: string;
}